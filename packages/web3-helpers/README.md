Scripts collection for Web3 development

## How to use them?
- Install [Volta](https://volta.sh)
- Link scripts by running
  ```bash
  yarn workspace web3-helpers link
  ```
- Run a script
  ```bash
  yarn workspace web3-helpers exec sendNativeAssetForEvmBlockchains
  ```

### sendNativeAssetForEvmBlockchains
```bash
yarn workspace web3-helpers exec sendNativeAssetForEvmBlockchains NETWORK \
INFURA_API_KEY \
SOURCE_PRIVATE_KEY \
TARGET_ADDRESS \
AMOUNT \
ASSET
```