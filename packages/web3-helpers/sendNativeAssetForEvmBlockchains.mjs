#!/usr/bin/env node
import { Web3 } from 'web3';

const args = process.argv.slice(2);

if (args.length !== 6) {
  console.log(
    'Usage: sendNativeAssetForEvmBlockchains <network> <apiKey> <sourcePrivateKey> <targetAddress> <amount> <asset>'
  );
  process.exit(1);
}

const [network, apiKey, sourcePrivateKey, targetAddress, amount, asset] = args;
const web3 = new Web3(
  new Web3.providers.HttpProvider(`https://${network}.infura.io/v3/${apiKey}`)
);
const signer = web3.eth.accounts.privateKeyToAccount(sourcePrivateKey);

web3.eth.accounts.wallet.add(signer);

const transactionBody = {
  from: signer.address,
  to: targetAddress,
  value: web3.utils.toWei(amount, asset),
};
transactionBody.gas = await web3.eth.estimateGas(transactionBody);

const receipt = await web3.eth
  .sendTransaction(transactionBody)
  .once('transactionHash', (transactionHash) => {
    console.log(`Mining transaction ...`);
    console.log(`Transaction hash: ${transactionHash}`);
  });

console.log(`Mined in block ${receipt.blockNumber}`);
